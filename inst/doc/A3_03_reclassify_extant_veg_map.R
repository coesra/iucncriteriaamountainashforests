
### name:A3_02_reclassify_extant_veg_map.R####
# func
setwd(projectdir)
source(file.path(codedir, "utils.R"))

# load
#datadirA303 <- "/home/virtual_home/public_share_data/IUCNmountainashforests_data/raw_data/nvis/aust4_1e_mvs2"
#matrix(dir(datadirA303))
#infileA303 <- "w001001.adf"
rasterOptions(maxmemory=1e+24)
dat_ext <- raster(file.path(datadirA303, infileA303))
#str(dat_ext)
E<-extent(ecosystem_bbox_albers)
#E <- extent(c(144, 150, -40, -35)) 
#E
dat_ext <- crop(dat_ext, E)
#str(dat_ext@data)
namclasses <- dat_ext@data@attributes[[1]]$MVS_NAME
matrix(namclasses)
index <-  which(namclasses %in% c("Eucalyptus (+/- tall) open forest with a dense broad-leaved and/or tree-fern understorey (wet sclerophyll)", "Mangroves"))
index
#dat_ext@data@attributes[[1]][index,]


# do
namlist <- as.data.frame(
  table(dat_ext@data@values)
                         )[,1]
str(namlist)
#dat_ext@data@attributes
#lapply(dat_ext@data@attributes[[1]], table)
#namlist  <- names(table(dat_ext@data@attributes[[1]]$MVS_NAME))
indicate <- rep(NA, length(namlist))
index
namlist
namlist <- as.numeric(as.character(namlist))
indicate[which(namlist %in% index)]  <- 1
indicate
isBecomes <- data.frame(cbind(namlist, indicate))
isBecomes
str(isBecomes)
ma_nvis_ext_crop <- subs(dat_ext, isBecomes)
table(ma_nvis_ext_crop@data@values)
#png("results/qc_nviz.png")
#plot(ma_nvis_ext_crop)
#title("ext_out")
#dev.off()
ma_nvis_ext_crop  <- rasterToPolygons(ma_nvis_ext_crop, fun=function(x){x>0})
#str(ma_nvis_ext_crop)
# this is a big thing!
getwd()
outdir  <- "data"
#dir.create(outdir)
setwd(outdir)
writeOGR(ma_nvis_ext_crop, "ma_nvis_ext_crop_p.shp", "ma_nvis_ext_crop_p", driver="ESRI Shapefile",
         overwrite_layer = TRUE)
ma_nvis_ext_crop2 <- gUnaryUnion(ma_nvis_ext_crop)
dat  <- as.data.frame("ecosystem_present")
names(dat) <- "V1"
ma_nvis_ext_crop2 <- SpatialPolygonsDataFrame(ma_nvis_ext_crop2,
                                                   data=dat,
                                                   match.ID=F)

writeOGR(ma_nvis_ext_crop2,
         "ma_nvis_ext_crop2.shp", "ma_nvis_ext_crop2",
         driver = "ESRI Shapefile",
         overwrite_layer = TRUE)
